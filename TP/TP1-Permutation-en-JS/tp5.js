// Fonction pour permuter les éléments d'un tableau
function permuter(array, index = 0, result = []) {
  // Si on est à la fin du tableau, on ajoute une copie du tableau
  if (index === array.length - 1) {
    result.push([...array]); // copie du tableau
  } else {
    for (let i = index; i < array.length; i++) {
      [array[index], array[i]] = [array[i], array[index]];
      permuter(array, index + 1, result);
      [array[index], array[i]] = [array[i], array[index]];
    }
  }
  return result;
}

// Affiche toutes les permutations des tableaux de 2 à 10 éléments
for (let n = 2; n <= 10; n++) {
  const array = Array.from({ length: n }, (_, i) => i + 1);
  const permutations = permuter(array);
  console.log(`Permutations pour le tableau [${array.join(", ")}] :`);
  console.log(permutations);
  console.log(`Nombre de permutations : ${permutations.length}`);
  console.log("");
}

// Nombre de permutations
// 2 éléments : 2
// 3 éléments : 6
// 4 éléments : 24
// 5 éléments : 120
// 6 éléments : 720
// 7 éléments : 5040
// 8 éléments : 40320
// 9 éléments : 362880
// 10 éléments : 3628800
