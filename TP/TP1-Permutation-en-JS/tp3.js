function iteration_swap(a, b) {
  let temp = a;
  a = b;
  b = temp;
}

function renverser(premier, dernier) {
  while (premier !== dernier && premier !== --dernier) {
    iteration_swap(premier++, dernier);
  }
}

function prochaine_permutation(premier, dernier) {
  if (premier === dernier) return false;
  let i = dernier;
  if (premier === --i) return false;
  while (true) {
    let i1, i2;
    i1 = i;
    if (--i < i1) {
      i2 = dernier;
      while (!(i < --i2));
      iteration_swap(i, i2);
      renverser(i1, dernier);
      return true;
    }
    if (i === premier) {
      renverser(premier, dernier);
      return false;
    }
  }
}

function main() {
  // 1ere methode
  let taille = 3;
  let t = [1, 2, 3];
  let cpt = 0;
  do {
    let i;
    for (i = 0; i < taille; ++i) {
      console.log(t[i] + " ");
    }
    console.log("\n");
    cpt++;
  } while (prochaine_permutation(t, t + taille));

  console.log("Nombre de permutations : " + cpt + "\n\n");

  return 0;
}
