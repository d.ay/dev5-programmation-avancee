function echangerElements(tableau, i, j) {
  const temporaire = tableau[i];
  tableau[i] = tableau[j];
  tableau[j] = temporaire;
}

function permuter(tableau, debut, fin, resultat) {
  if (debut === fin) {
    resultat.push([...tableau]); // copie du tableau
  } else {
    for (let i = debut; i <= fin; i++) {
      echangerElements(tableau, debut, i);
      permuter(tableau, debut + 1, fin, resultat);
      echangerElements(tableau, debut, i);
    }
  }
}

const monTableau = [1, 2, 3];
const permutations = [];
permuter(monTableau, 0, monTableau.length - 1, permutations);
console.log(permutations);
