const contfact = (n, cont) => {
  if (n == 0) return cont(1);
  else return contfact(n - 1, (x) => cont(n * x));
};

const contfactbis = (n, cont) => {
  if (n == 0) return cont;
  else return contfactbis(n - 1, (x) => cont(n * x));
};

contfact(0, (x) => console.log(x)); // 1
contfact(1, (x) => console.log(x)); // 1
contfact(2, (x) => console.log(x)); // 2
contfact(3, (x) => console.log(x)); // 6
contfact(4, (x) => console.log(x)); // 24
contfact(5, (x) => console.log(x)); // 120
contfact(6, (x) => console.log(x)); // 720
contfact(7, (x) => console.log(x)); // 5040
contfact(8, (x) => console.log(x)); // 40320
contfact(9, (x) => console.log(x)); // 362880
contfact(10, (x) => console.log(x)); // 3628800

contfactbis(0, 1); // 1
contfactbis(1, 1); // 1
contfactbis(2, 1); // 2
contfactbis(3, 1); // 6
