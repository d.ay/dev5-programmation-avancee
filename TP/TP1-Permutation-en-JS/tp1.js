// afficher une fonction qui affiche hello world
function helloWorld() {
  console.log("Hello World");
}
helloWorld();

let a = 1;
function g(x) {
  let a = 2;
  return a + x;
}
console.log(g(2));

let x = 1;
let y = 2;
{
  let x = y;
  //let y=x;
  console.log(x + " " + y);
}

let x1 = 0;
{
  let y = x + 1;
  let x1 = 99;
  console.log(x + y);
}
let x2 = 0;
{
  let y = x + 1;
  let z = 99;
  console.log(z + y);
}

let h2 = (f, x) => {
  let y = f(x);
  let z = f(y);
  return (y + z) / (y - z);
};
function f(x) {
  return 2 * x;
}
console.log(h2(f, 3));

const fact2 = (n) => {
  if (n == 0) {
    return 1;
  } else {
    return n * fact(n - 1);
  }
};
let t = fact(5);
console.log(t);
