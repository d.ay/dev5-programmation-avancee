#include <stdio.h>
//  gcc -Wall exo.c -o exo
//  ./exo

int denombre_chemins(int n, int m) {
    int dp[n][m];
    
    // Initialisation des bords supérieur et gauche
    for (int i = 0; i < n; i++)
        dp[i][0] = 1;
    
    for (int j = 0; j < m; j++)
        dp[0][j] = 1;
    
    // Calcul des chemins pour chaque cellule
    for (int i = 1; i < n; i++) {
        for (int j = 1; j < m; j++) {
            dp[i][j] = dp[i-1][j] + dp[i][j-1];
            printf("dp[%d][ %d] = %d\n", i, j, dp[i][j]);
        }
    }
    
    return dp[n-1][m-1];
}

int main() {
    int n = 3; // nombre de lignes
    int m = 4; // nombre de colonnes
    
    int nombre_chemins = denombre_chemins(n, m);
    
    printf("Le nombre de chemins possibles est : %d\n", nombre_chemins);
    
    return 0;
}
