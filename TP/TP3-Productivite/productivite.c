// 15 mai 2023
//  gcc -Wall productivite.c -o productivite
//  ./productivite


int tabProductivite[5][4]={
    {0, 0, 0, 0},
    {26, 23, 16, 19},
    {39, 36, 32, 36},
    {48, 44, 48, 47},
    {54, 49, 64, 56}
};


int optimise(int tab[5][4], int L, int C){
    int res=0;
    int a,b,c,d;
    int v;
    int i,j,k,l;
    for(i=0;i<L;i++){
        for(j=0;j<L;j++){
            for(k=0;k<L;k++){
                for(l=0;l<L;l++){
                    if((i+j+k+l)==4){
                        v=tab[i][0]+tab[j][1]+tab[k][2]+tab[l][3];
                        if(v>res){
                            res=v;
                            a=i;
                            b=j;
                            c=k;
                            d=l;
                        }
                    }
                }
            }
        }
    }
    printf("%d\n",res);
    printf("\t%d\t%d\t%d\t%d\n",a,b,c,d);
    return res;
}


int main(int argc, char **argv){
    int res;
    res=optimise(tabProductivite,5,4);
    exit(0);
}

