def fiboDeBase(n):
    if n == 0:
        return 1
    elif n == 1:
        return 1
    else:
        return fiboDeBase(n-1) + fiboDeBase(n-2)
    

def fiboMemorize(n, M):
    if M[n] == -1:
        if n == 0:
            M[n] = 1
        elif n == 1:
            M[n] = 1
        else:
            M[n] = fiboMemorize(n-1, M) + fiboMemorize(n-2, M)
    return M[n]

def fiboMemoProf(n,M):
    if M[n] == -1:
        M[n] = fiboMemoProf(n-1,M) + fiboMemoProf(n-2,M)
        return M[n]
    else:
        return M[n]
    
n=37
T = [-1]*(n+1)
T[0] = 1
T[1] = 1
print("Test fibo ordre "+str(n))
print(fiboDeBase(n)) # 39088169
print(fiboMemorize(n, T)) # 39088169
print(fiboMemoProf(n, T)) # 39088169


def distanceMots(u,v) : 
    if u == "" :
        return len(v)
    elif v == "" :
        return len(u)
    else :
        if u[0] == v[0] :
            return distanceMots(u[1:],v[1:])
        else :
            return 1 + min(distanceMots(u[1:],v), distanceMots(u,v[1:]), distanceMots(u[1:],v[1:]))
        

def distanceMotsMemo(u,v,M) :
    if M[len(u)][len(v)] == -1 :
        if u == "" :
            M[len(u)][len(v)] = len(v)
        elif v == "" :
            M[len(u)][len(v)] = len(u)
        else :
            if u[0] == v[0] :
                M[len(u)][len(v)] = distanceMotsMemo(u[1:],v[1:],M)
            else :
                M[len(u)][len(v)] = 1 + min(distanceMotsMemo(u[1:],v,M), distanceMotsMemo(u,v[1:],M), distanceMotsMemo(u[1:],v[1:],M))
    return M[len(u)][len(v)]

def distanceProfPasOpti(u,v) :
    if u == "" :
        return len(v)
    elif v == "" :
        return len(u)
    elif u[0] == v[0] :
        return distanceProfPasOpti(u[1:],v[1:])
    else :
        return 1 + min(distanceProfPasOpti(u[1:],v), distanceProfPasOpti(u,v[1:]), distanceProfPasOpti(u[1:],v[1:]))



#appel
u="chien"
v="chine"
M = [[-1]*(len(v)+1) for i in range(len(u)+1)]
print("Test distanceMots")
print(distanceMots(u,v)) # 2
print(distanceMotsMemo(u,v,M)) # 2
print(distanceProfPasOpti("bato","martoi")) # 3
print(distanceProfPasOpti("grenouille","platdenouilles")) # 6
print(distanceProfPasOpti("chien","chine")) # 2
print(distanceProfPasOpti("citrouille","citron")) # 5
print(distanceProfPasOpti("chien","chat")) # 3
print(distanceProfPasOpti("soleil","lune")) # 5
print(distanceProfPasOpti("bonjour","bonsoir")) # 2
print(distanceProfPasOpti("bateau","bateaux")) # 1
print(distanceProfPasOpti("maison","raison")) # 1
print(distanceProfPasOpti("ordinateur","chocolat")) # 9




def initMemoMots(u,v) :
    m = len(u)
    n = len(v)
    ligne = [-1]*(n+1)
    M = [ligne]*(m+1)
    for i in range(m+1) :
        M.append(ligne[:])
    for i in range(m+1) :
        M[i][n] = m-i
    for j in range(n+1) :
        M[m][i] = n-i
    return M

print("Test distanceMotsMemo")
M = initMemoMots("bato","martoi")
print(M)

def distanceMotsIter(u,v) : 
    m=len(u)
    n=len(v)
    M = initMemoMots(u,v)
    for i in range(m-1,-1,-1) :
        for j in range(n-1,-1,-1) :
            if u[i] == v[j] :
                M[i][j] = M[i+1][j+1]
            else :
                M[i][j] = 1 + min(M[i+1][j], M[i][j+1], M[i+1][j+1])
    return M[0][0]

print(distanceMotsIter("bato","martoi")) # 3
print(distanceMotsIter("grenouille","platdenouilles")) # 6
print(distanceMotsIter("chien","chine")) # 2

def enveloppeConvexe(L) :
    if len(L) == 1 :
        return L
    else :
        return enveloppeConvexeRec(L, L[0], L[1])
    
def enveloppeConvexeRec(L, p1, p2) :
    if len(L) == 0 :
        return [p1,p2]
    else :
        p = L[0]
        if produitVectoriel(p1,p2,p) < 0 :
            return enveloppeConvexeRec(L[1:], p1, p)
        else :
            return enveloppeConvexeRec(L[1:], p1, p2)
        
def produitVectoriel(p1,p2,p3) :
    return (p2[0]-p1[0])*(p3[1]-p1[1]) - (p2[1]-p1[1])*(p3[0]-p1[0])

def enveloppeConvexeIter(L) :
    if len(L) == 1 :
        return L
    else :
        L.sort()
        p1 = L[0]
        p2 = L[1]
        H = [p1,p2]
        for i in range(2,len(L)) :
            p = L[i]
            while produitVectoriel(H[-2],H[-1],p) < 0 :
                H.pop()
            H.append(p)
        return H
    
def enveloppeConvexeIter2(L) :
    if len(L) == 1 :
        return L
    else :
        L.sort()
        p1 = L[0]
        p2 = L[1]
        H = [p1,p2]
        for i in range(2,len(L)) :
            p = L[i]
            while produitVectoriel(H[-2],H[-1],p) < 0 :
                H.pop()
            H.append(p)
        p1 = L[-1]
        p2 = L[-2]
        H2 = [p1,p2]
        for i in range(len(L)-3,-1,-1) :
            p = L[i]
            while produitVectoriel(H2[-2],H2[-1],p) < 0 :
                H2.pop()
            H2.append(p)
        H2.pop(0)
        H2.pop()
        H.extend(H2)
        return H
    

# appel
L = [(0,0),(1,1),(2,0),(3,1),(4,0),(5,1),(6,0),(7,1),(8,0),(9,1),(10,0),(11,1),(12,0),(13,1),(14,0),(15,1),(16,0),(17,1),(18,0),(19,1),(20,0),(21,1),(22,0),(23,1),(24,0),(25,1),(26,0),(27,1),(28,0),(29,1),(30,0),(31,1),(32,0),(33,1),(34,0),(35,1),(36,0),(37,1),(38,0),(39,1),(40,0),(41,1),(42,0),(43,1),(44,0),(45,1),(46,0),(47,1),(48,0),(49,1),(50,0),(51,1),(52,0),(53,1),(54,0),(55,1),(56,0),(57,1),(58,0),(59,1),(60,0),(61,1),(62,0),(63,1),(64,0),(65,1),(66,0),(67,1),(68,0),(69,1),(70,0),(71,1),(72,0),(73,1),(74,0),(75,1),(76,0),(77,1),(78,0),(79,1),(80,0),(81,1),(82,0),(83,1),(84,0),(85,1),(86,0),(87,1),(88,0),(89,1),(90,0),(91,1),(92,0),(93,1),(94,0),(95,1),(96,0),(97,1),(98,0),(99,1),(100,0),(101,1),(102,0),(103,1),(104,0),(105,1),(106,0),(107,1),(108,0),(109,1),(110,0),(111,1),(112,0),(113,1),(114,0),(115,1),(116,0),(117,1),(118,0),(119,1),(120,0),(121,1),(122,0),(123,1)]
print(enveloppeConvexe(L))
