#RECTANGLE 
def pgrectangleNaif(d) :
    res = (0,0, d[0])
    for i in range(len(d)) :
        for j in range(i, len(d)) :
            hmin = min(d[i:j+1])
            s = hmin * (j + 1 - i)
            if s > res[2] :
                res = (i, j, s)
    return res

data = [3,2,3,4,1,3,5,6,4,3]

print(pgrectangleNaif(data))


def pgrectangle(d) :
    res = (0,0, d[0])
    for i in range(len(d)) :
        hmin = d[i]
        for j in range(i, len(d)) :
            hmin = min(hmin, d[j])
            s = hmin * (j + 1 - i)
            if s > res[2] :
                res = (i, j, s)
    return res

data = [3,2,3,4,1,3,5,6,4,3]

print(pgrectangle(data))


def pgrectangleDPR(d,i,j) :
    if i > j : return (0,0,0)

    hmin = d[i]

    k = i

    for p in range(i,j+1) :
        if d[p] < hmin :
            k = p
            hmin = d[p]
    s = hmin * (j + 1 - i)

    droite = pgrectangleDPR(d,k+1,j)
    gauche = pgrectangleDPR(d,i,k)
    if s >= droite[2] and s >= gauche[2] : return (i,j,s)
    elif droite[2] >= s and droite[2] >= gauche[2] : return droite
    else : return gauche

print(pgrectangleDPR(data,0,len(data)-1))




def pgrectangle_diviser_pour_regner(hist):
    return _pgrectangle_diviser_pour_regner(hist, 0, len(hist) - 1)

def _pgrectangle_diviser_pour_regner(hist, debut, fin):
    if debut > fin:
        return (0, -1, -1)
    if debut == fin:
        return (debut, debut, hist[debut])
    milieu = (debut + fin) // 2
    max_gauche = _pgrectangle_diviser_pour_regner(hist, debut, milieu)
    max_droit = _pgrectangle_diviser_pour_regner(hist, milieu + 1, fin)
    max_croisement = _pgrectangle_croisement(hist, debut, milieu, fin)
    if max_gauche[2] >= max_droit[2] and max_gauche[2] >= max_croisement[2]:
        return max_gauche
    elif max_droit[2] >= max_gauche[2] and max_droit[2] >= max_croisement[2]:
        return max_droit
    else:
        return max_croisement

def _pgrectangle_croisement(hist, debut, milieu, fin):
    gauche = milieu
    droite = milieu + 1
    hauteur_gauche = hist[gauche]
    hauteur_droite = hist[droite]
    largeur = 1
    max_rect = (gauche, droite, hauteur_gauche + hauteur_droite)
    while gauche >= debut and droite <= fin:
        if hauteur_gauche < hauteur_droite:
            droite += 1
            if droite <= fin:
                largeur += 1
                hauteur_droite = min(hauteur_droite, hist[droite])
                s = largeur * hauteur_droite
                if s > max_rect[2]:
                    max_rect = (gauche, droite, s)
            else:
                break
        else:
            gauche -= 1
            if gauche >= debut:
                largeur += 1
                hauteur_gauche = min(hauteur_gauche, hist[gauche])
                s = largeur * hauteur_gauche
                if s > max_rect[2]:
                    max_rect = (gauche, droite, s)
            else:
                break
    return max_rect


print(pgrectangle_diviser_pour_regner(data)) # (4, 9, 15)




