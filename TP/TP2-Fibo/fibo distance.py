def fiboDeBase(n) :
    if n==0 : return 1
    elif n==1 : return 1
    else : return fiboDeBase(n-1) + fiboDeBase(n-2)

def fiboMemo(n,M) :
    if M[n] == -1 :
        M[n] = fiboMemo(n-1, M) + fiboMemo(n-2,M)
        return M[n]
    else :
        return M[n]
    

n=10
T=[-1]*(n+1)
T[0]=1
T[1]=1
print("Test Fibo ordre "+str(n))
print(fiboDeBase(n))
print(fiboMemo(n,T))


def distanceMots(u,v) :
    if u=="" : return len(v)
    elif v=="" : return len(u)
    elif u[0]==v[0] : return distanceMots(u[1:], v[1:])
    else : return 1 + min(distanceMots(u[1:], v[1:]),
                          distanceMots(u, v[1:]),
                          distanceMots(u[1:], v))
    
def initMemoMots(u,v) :
    m = len(u)
    n = len(v)
    ligne=[-1]*(n+1)
    M=[]
    for i in range(m+1):
        M.append(ligne[:])
    for i in range(m+1):
        M[i][n]=m-i
    for i in range(n+1):
        M[m][i]=n-i
    return M

print(distanceMots("bato","martoi"))
print(distanceMots("grenouille","platdenouilles"))

T = initMemoMots("bato","martoi")
print(T)

def distanceMotsIter(u,v):
    m = len(u)
    n = len(v)
    T = initMemoMots(u,v)
    for i in range(m-1,-1,-1) :
        for j in range(n-1,-1,-1) :
            if u[i]==v[j] : T[i][j]=T[i+1][j+1]
            else : T[i][j]=1+ min(T[i+1][j+1], T[i][j+1],T[i+1][j])
    return T

d = distanceMotsIter("bato","martoi")
print(d)
d = distanceMotsIter("grenouille","platdenouilles")
print(d)


def aligne(u,v) :
    T = distanceMotsIter(u,v)
    print(T[0][0])
    i=0
    j=0 # T[i][j] = case courante
    while ((i<= len(u)) and (j<=len(v))) :
    # remontée dans la table
        if i==len(u) and j ==len(v) : 
            break
        elif i==len(u) : 
            print(".",v[j])
            j=j+1
        elif j==len(v) : 
            print(u[i],".")
            i=i+1
        elif u[i]==v[j] :
            print(u[i],v[j])
            (i,j)=(i+1,j+1) #pas de transformation
        elif T[i][j]==1+T[i+1][j+1]:
            print(u[i],v[j])
            (i,j)=(i+1,j+1)
        elif T[i][j]==1+T[i][j+1] : 
            print(".",v[j])
            j=j+1
        elif T[i][j]==1+T[i+1][j]:
            print(u[i],".")
            i=i+1


aligne("bato","martoi")
aligne("grenouille","platdenouilles")


