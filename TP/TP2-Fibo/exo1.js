const matrix = [
  [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
  [0, 0, 0, 0, 0, 0, 1, 1, 0, 0],
  [0, 0, 0, 1, 0, 0, 1, 1, 1, 0],
  [1, 0, 1, 1, 0, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 0, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
];

function maximalRectangle(matrix) {
  function maxArea(heights) {
    const stack = [-1];
    let area = 0;
    for (let i = 0; i < heights.length; i++) {
      while (
        stack[stack.length - 1] !== -1 &&
        heights[stack[stack.length - 1]] >= heights[i]
      ) {
        area = Math.max(
          area,
          heights[stack.pop()] * (i - stack[stack.length - 1] - 1)
        );
      }
      stack.push(i);
    }
    while (stack[stack.length - 1] !== -1) {
      area = Math.max(
        area,
        heights[stack.pop()] * (heights.length - stack[stack.length - 1] - 1)
      );
    }
    return area;
  }

  if (!matrix || !matrix.length) {
    return 0;
  }

  const m = matrix.length;
  const n = matrix[0].length;
  let heights = Array(n).fill(0);
  let maxAreaRect = 0;

  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (matrix[i][j] === 1) {
        heights[j]++;
      } else {
        heights[j] = 0;
      }
    }
    maxAreaRect = Math.max(maxAreaRect, maxArea(heights));
  }

  return maxAreaRect;
}
