nbAppelFibo = 0
def fibo(n) :
    if n == 0 :
        return 0
    elif n == 1 :
        return 1
    else :
        return fibo(n-1) + fibo(n-2)
    


def fiboMemorize(n,M) :
    if M[n] == -1 :
        if n == 0 :
            M[n] = 0
        elif n == 1 :
            M[n] = 1
        else :
            M[n] = fiboMemorize(n-1,M) + fiboMemorize(n-2,M)

#appel
n=50
M = [-1]*(n+1)
M[0] = 0
M[1] = 1
f= fiboMemorize(n,M)


# def fiboIteratif(n, M) :
#     for i in range(2,n+1) :
#     M[i] = M[i-1] + M[i-2]
#     return M[n]

#appel
n=50
M = [-1]*(n+1)
M[0] = 0
M[1] = 1
#f= fiboIteratif(n,M)

def fiboIteratif2(n) :
    if n <+ 1 :
        return 0
    v0=0
    v1=1
    for i in range(2,n+1) :
        res=v0+v1
        v0=v1
        v1=res
    return res