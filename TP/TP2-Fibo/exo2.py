# A l’issue de chaque journée de championnat un classement est établi. En calculant le nombre 
# d’inversions apparues dans 2 classements consécutifs on pourra disposer d’un indicateur sur la 
# stabilité du classement.
# Chaque concurrent est représenté par un nombre entier, qui est son classement à l’issue de la 
# journée n-1.
# Exemple avec 12 concurrents.
# Classement après la journée n-1 : [01 ; 02 ; 03 ; 04 ; 05 ; 06 ; 07 ; 08 ; 09 ; 10 ; 11 ; 12]
# Supposons qu’après la journée n le classement ait été modifié de la façon suivante.
# Classement après la journée n : [03 ; 02 ; 01 ; 04 ; 08 ; 06 ; 07 ; 11 ; 09 ; 10 ; 05 ; 12]
# Les inversions sont au nombre de 13 : 03-02, 03-01, 02-01, 08-06, 08-07, 08-05, 06-05, 07-05, 
# 11-09, 11-10, 11-05, 09-05, 10-05.
# 1) Formalisez les inversions.
# 2) Donnez un algorithme naïf permettant de compter les inversions.
# 3) Quelle est sa complexité ?
# 4) Expliquez quelle approche « Diviser pour mieux régner » peut être envisagée pour améliorer 
# la complexité.
# 5) Donnez l’algorithme DpR 


# VERSION NAIVE
data2 = [3,2,1,4,8,6,7,11,9,10,5,12]

def nbInversionsNaif(d) :
    res = 0
    for i in range(len(d)-1) :
        for j in range(i+1, len(d)) :
            if d[i] > d[j] :
                res += 1
    return res

print(nbInversionsNaif(data2))


# MA VERSION DPR
def compter_inversions_dpr(T):
    n = len(T)
    if n <= 1:
        return 0
    T1 = T[:n//2]
    T2 = T[n//2:]
    inversions = compter_inversions_dpr(T1) + compter_inversions_dpr(T2)
    i = j = 0
    T_ = []
    while i < len(T1) and j < len(T2):
        if T1[i] <= T2[j]:
            T_.append(T1[i])
            i += 1
        else:
            T_.append(T2[j])
            j += 1
            inversions += len(T1) - i
    T_ += T1[i:]
    T_ += T2[j:]
    T[:] = T_[:]
    return inversions

print(compter_inversions_dpr(data2))

# VERSION PROF
def nbInversionsDPR(d) :
    res = 0
    d1 = d[:len(d) // 2]
    d2 = d[len(d) // 2:]
    n1 = nbInversionsDPR(d1) + nbInversionsDPR(d2)
    d1.sort()
    d2.sort()
    i= 0
    for j in range(len(d2)) :
        c=1
        while(c) :
            if(i>=len(d1)) :
                c=0
            elif(d1[i] > d2[j]) :
                res += len(d1)-i
                c=0
            else :
                i+=1
    return res + n1

print(nbInversionsDPR(data2))


def inversionsDPR(tab1,tab2):
    if len(tab1) == 1 and len(tab2) == 1:
        if tab1[0] > tab2[0]:
            return 1
        else:
            return 0
    else:
        tab1_1 = tab1[:len(tab1)//2]
        tab1_2 = tab1[len(tab1)//2:]
        tab2_1 = tab2[:len(tab2)//2]
        tab2_2 = tab2[len(tab2)//2:]
        return inversionsDPR(tab1_1,tab2_1) + inversionsDPR(tab1_2,tab2_2) + inversionsDPR(tab1_1,tab2_2) + inversionsDPR(tab1_2,tab2_1)


#print(inversionsDPR(tab1,tab2)) 




