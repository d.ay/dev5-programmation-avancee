function d(u, i, v, j, T) {
  if (T[i][j] == -1) {
    if (u.slice(i) == "") T[i][j] = v.slice(j).length;
    else if (v.slice(j) == "") T[i][j] = u.slice(i).length;
    else if (u[i] == v[j]) T[i][j] = d(u, i + 1, v, j + 1, T);
    else
      T[i][j] =
        1 +
        Math.min(
          d(u, i + 1, v, j, T),
          d(u, i, v, j + 1, T),
          d(u, i + 1, v, j + 1, T)
        );
  }
  return T[i][j];
}
