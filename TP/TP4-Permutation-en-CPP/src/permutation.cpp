#include <iostream>
#include <algorithm>
 
using namespace std;


void iteration_swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void renverser(int *premier, int *dernier)
{
    while ((premier != dernier) && (premier != --dernier))
    {
        iteration_swap(premier++, dernier);
    }
}

bool prochaine_permutation(int *premier, int *dernier)
{
    if (premier == dernier)
        return false;
    int *i = dernier;
    if (premier == --i)
        return false;
    while (true)
    {
        int *i1, *i2;
        i1 = i;
        if (*--i < *i1)
        {
            i2 = dernier;
            while (!(*i < *--i2))
                ;
            iteration_swap(i, i2);
            renverser(i1, dernier);
            return true;
        }
        if (i == premier)
        {
            renverser(premier, dernier);
            return false;
        }
    }
}


int main(void)
{
    // 1ere methode
    int taille = 3;
    int t[taille] = {1, 2, 3};
    int cpt = 0;
    do
    {
        int i;
        for (i=0; i<taille; ++i){
            cout << t[i] << " ";
        }
        cout << endl;
        cpt++;
    }
    while (prochaine_permutation(t, t+taille));

    cout << "Nombre de permutations : " << cpt << endl;
    cout << endl << endl;

    return 0;
}

