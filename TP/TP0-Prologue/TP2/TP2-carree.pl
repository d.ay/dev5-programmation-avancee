carreMagique(A,B,C,D,E,F,G,H,I):-
    permutation([1,2,3,4,5,6,7,8,9], [A,B,C,D,E,F,G,H,I]),
    15 is A+B+C, 
    15 is D+E+F,
    15 is G+H+I,
    15 is A+D+G,
    15 is B+E+H,
    15 is C+F+I,
    15 is A+E+I,
    15 is C+E+G,
    write(A), write(B), write(C), nl,
    write(D), write(E), write(F), nl,
    write(G), write(H), write(I), nl.


asOrder(Carreau,Pique,Coeur,Trefle):-
    permutation([1,2,3,4], [Carreau,Pique,Coeur,Trefle]),
    Pique \=2,
    Trefle \=2,
    Trefle > Carreau,
    not(Carreau is Coeur+1), not(Coeur is Carreau+1).

asOrder2(Carreau,Pique,Coeur,Trefle):-
    permutation([1,2,3,4], [Carreau,Pique,Coeur,Trefle]),
    Pique \=2,
    Trefle \=2,
    Trefle > Carreau,
    V=Carreau+1,
    N=Coeur+1,
    V\=Coeur,
    N\=Carreau.
