
einstein(A,B,C,D,E) :- 
    permutation([1, 2, 3, 4, 5], [ROUGE,VERT,BLANC,JAUNE,BLEU]),
    VERT < BLANC,
    permutation([1, 2, 3, 4, 5], [ANGLAIS,SUEDOIS,DANOIS,NORVEGIEN,ALLEMAND]),
    ANGLAIS=ROUGE, NORVEGIEN\=BLEU,
    permutation([1, 2, 3, 4, 5], [PALLMALL,DUNHILL,BLEND,PHILIPPMORRIS, PRINCE]),
    DUNHILL=JAUNE, ALLEMAND=PRINCE,
    permutation([1, 2, 3, 4, 5], [THE,CAFE,LAIT,BIERE,EAU]),
    DANOIS=THE, VERT=CAFE, PHILIPPMORRIS=BIERE,
    permutation([1, 2, 3, 4, 5], [CHIEN, OISEAUX,CHAT,CHEVAUX, POISSON]),
    SUEDOIS=CHIEN, PALLMALL=OISEAUX, BLEND\=CHAT, CHEVAUX\=DUNHILL,
    write(POISSON), nl,
    write(ANGLAIS), write(ROUGE), write(BLANC), write(JAUNE), write(BLEU), nl,
    write(PALLMALL), write(DUNHILL), write(BLEND), write(PHILIPPMORRIS), write(PRINCE), nl,
    write(THE), write(CAFE), write(LAIT), write(BIERE), write(EAU), nl,
    write(CHIEN), write(OISEAUX), write(CHAT), write(CHEVAUX), write(POISSON), nl.
    