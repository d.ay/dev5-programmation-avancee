listing

chien(rex).
chien(medor).

chien(X) :- chien(X).

file consult 
listing(chien).
?- listing(chien).
chien(rex).
chien(medor).
chien(ratanplan).
chien(milou).


Pour poser des questions 
?- chien(X).
Mettre des points virgules
X = rex ;
X = medor ;
X = ratanplan ;
X = milou.

Pour poser des question
?- chien(toto).
false

?- chien(milou).
true.

?- chien(toto).
false.

?- findall(X,chien(x),L).
L = [].

?- findall(X,chien(x),L).
L = [].

Récupérer la liste des chiens
?- findall(X,chien(X),L).
L = [rex, medor, ratanplan, milou]


?- chien(X),write(X),write(' '),fail.
rex medor ratanplan milou 


EXERCICE 1
Est-ce que Paul est un homme ?
?- homme(paul).
true.

Est-ce que Benoit est une femme ?
?- femme(benoit).
false.

Qui est une femme ?
?- femme(X).
X = ginette ;
X = germaine ;
X = christiane ;
X = simone ;
X = marie ;
X = sophie.

Est ce que Marie est la mere de sophie ?
?- mere(marie,sophie).
true.


?- mere(marie,sophie).
true.

?- mere(marie,benoit).
false.


ഀ?- mere(X,jean).
X = germaine.

?- pere(paul,X),write(X),write(' '),fail.
bertrand sophie 
false.


?- pere(paul,X),write(X),write(' '),fail.
bertrand sophie 
false.

?- pere(X,Y).
X = albert,
Y = jean ;
X = jean,
Y = paul ;
X = paul,
Y = bertrand ;
X = paul,
Y = sophie ;
X = jean,
Y = simone ;
X = louis,
Y = benoit ;
X = henri,
Y = bob ;
X = riton,
Y = tom.

?- findall(X,pere(hf,X),L.
ERROR: Syntax error: Operator expected
ERROR: findall(X,pere(hf,X),L
ERROR: ** here **
ERROR


?- homme(X),pere(X,Y).
X = albert,
Y = jean ;
X = jean,
Y = paul ;
X = jean,
Y = simone ;
X = paul,
Y = bertrand ;
X = paul,
Y = sophie ;
X = louis,
Y = benoit ;
X = henri,
Y = bob ;
X = riton,
Y = tom.

?- homme(X),pere(X,_).
X = albert ;
X = jean ;
X = jean ;
X = paul ;
X = paul ;
X = louis ;
X = henri ;
X = riton.


PAS DE REGLE DANS LINTERPRETEUR
parent(X,Y) :- pere(X,Y).
parent(X,Y) :- mere(X,Y).


?- parent(X,Y),homme(X).
X = albert,
Y = jean ;
X = jean,
Y = paul ;
X = paul,
Y = bertrand ;
X = paul,
Y = sophie ;
X = jean,
Y = simone ;
X = louis,
Y = benoit ;
X = henri,
Y = bob ;
X = riton,
Y = tom ;
false.


?- pere(X,Z),parent(Z,Y).
X = albert,
Z = jean,
Y = paul ;
X = albert,
Z = jean,
Y = simone ;
X = jean,
Z = paul,
Y = bertrand ;
X = jean,
Z = paul,
Y = sophie ;
X = jean,
Z = simone,
Y = benoit ;
false.



?- mere(X,Z),parent(Z,Y).
X = germaine,
Z = jean,
Y = paul ;
X = germaine,
Z = jean,
Y = simone ;
X = christiane,
Z = simone,
Y = benoit ;
X = christiane,
Z = paul,
Y = bertrand ;
X = christiane,
Z = paul,
Y = sophie ;
false.



?- homme(X),homme(Z),parent(Z,Y),parent(Z,X).
X = Y, Y = jean,
Z = albert ;
X = Y, Y = paul,
Z = jean ;
X = paul,
Z = jean,
Y = simone ;
X = Y, Y = bertrand,
Z = paul ;
X = bertrand,
Z = paul,
Y = sophie ;
X = Y, Y = benoit,
Z = louis ;
X = Y, Y = bob,
Z = henri ;
X = Y, Y = tom,
Z = riton ;
false.







EXERCICE 2

couleur(rouge).
couleur(bleu).
couleur(vert).
couleur(jaune).

colorier(A,B,C,D,E,F) :- couleur(A), couleur(B), couleur(C), couleur(D), couleur(E), couleur(F), A \== B, B \== C, A \== C,  A \== D,  B \== D,  A \== E, D \== E, A \== F, C \== F, E \== F.

% P for predicate

count(P,Count) :-
        findall(1,P,L),
        length(L,Count).



-----------------------------------------------
