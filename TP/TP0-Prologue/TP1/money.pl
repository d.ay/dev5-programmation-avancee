valeur(1).
valeur(2).
valeur(3).
valeur(4).
valeur(5).
valeur(6).
valeur(7).
valeur(8).
valeur(9).

sol(S,E,N,D,M,O,R,Y) :-
    valeur(S),
    valeur(E),
    valeur(N),
    valeur(D),
    valeur(M),
    valeur(O),
    valeur(R),
    valeur(Y),
    SEND is 1000*S + 100*E+ 10*N + 1*D,
    MORE is 1000*M + 100*O+ 10*R + 1*E,
    MONEY is 10000*M + 1000*O+ 100*N + 10*E + 1*Y,
    MONEY is SEND + MORE,
    S\=E,S\=N,S\=D,S\=M,S\=O,S\=R,S\=Y,
    E\=N,E\=D,E\=M,E\=O,E\=R,E\=Y,
    N\=D,N\=M,N\=O,N\=R,N\=Y,
    D\=M,D\=O,D\=R,D\=Y,
    M\=O,M\=R,M\=Y,
    O\=R,O\=Y,
    R\=Y.