valeur(0).
valeur(1).
valeur(2).
valeur(3).
valeur(4).
valeur(5).
valeur(6).
valeur(7).
valeur(8).
valeur(9).

solution(S,E,N,D,M,O,R,Y) :-
    valeur(S),
    valeur(E),
    valeur(N),
    valeur(D),
    valeur(M),
    valeur(O),
    valeur(R),
    valeur(Y),
    SEND is 1000*S + 100*E+ 10*N + 1*D,
    MORE is 1000*M + 100*O+ 10*R + 1*E,
    MONEY is 10000*M + 1000*O+ 100*N + 10*E + 1*Y,
    MONEY is SEND + MORE,
    write(SEND), write('+'), write(MORE), write('='), 
    write(MONEY),write(' '),fail.

solution2(S,E,N,D,M,O,R,Y) :- 
    valeur(S), 
    valeur(E), S\=E, 
    valeur(N), S\=N, E\=N,
    valeur(D), S\=D, E\=D, N\=D,
    valeur(M), S\=M, E\=M, N\=M, D\=M, M\=0,
    valeur(O), S\=O, E\=O, N\=O, D\=O, M\=O,
    valeur(R), S\=R, E\=R, N\=R, D\=R, M\=R, O\=R,
    valeur(Y), S\=Y, E\=Y, N\=Y, D\=Y, M\=Y, O\=Y, R\=Y,
    SEND is 1000*S + 100*E+ 10*N + 1*D,
    MORE is 1000*M + 100*O+ 10*R + 1*E,
    MONEY is 10000*M + 1000*O+ 100*N + 10*E + 1*Y,
    MONEY is SEND + MORE,
    write(SEND), write('+'), write(MORE), write('='), 
    write(MONEY),write(' '),fail.

solution3(S,E,N,D,M,O,R,Y) :- 
    permutation([0,1,2,3,4,5,6,7,8,9], [S,E,N,D,M,O,R,Y,A,B]),
    M\=0,
    SEND is 1000*S + 100*E+ 10*N + 1*D,
    MORE is 1000*M + 100*O+ 10*R + 1*E,
    MONEY is 10000*M + 1000*O+ 100*N + 10*E + 1*Y,
    MONEY is SEND + MORE,
    write(SEND), write('+'), write(MORE), write('='), 
    write(MONEY),write(' '),fail.