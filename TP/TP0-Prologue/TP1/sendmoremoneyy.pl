chiffre(0).
chiffre(1).
chiffre(2).
chiffre(3).
chiffre(4).
chiffre(5).
chiffre(6).
chiffre(7).
chiffre(8).
chiffre(9).

solution(S,E,N,D,M,O,R,Y) :- M=1, chiffre(S), chiffre(E), chiffre(N), chiffre(D), chiffre(O), chiffre(R), chiffre(Y),
    Send is 1000*S + 100*E + 10*N + D,
    More is 1000*M + 100*O + 10*R + E,
    Money is 10000 * M + 1000*O + 100*N + 10*E + Y,
    Money is Send + More,
    write(Send), write('+'), write(More), write('='), write(Money).
