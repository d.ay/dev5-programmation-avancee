:- use_module(library(clpfd)).
% faits
chiffre(0).
chiffre(1).
chiffre(2).
chiffre(3).
chiffre(4).
chiffre(5).
chiffre(6).
chiffre(7).
chiffre(8).
chiffre(9).

% regles
sendMoreMoney(S,E,N,D,M,O,R,Y) :- M=1, chiffre(S), chiffre(E), chiffre(N), chiffre(D), chiffre(O), chiffre(R), chiffre(Y),
    Send is 1000*S + 100*E + 10*N + D,
    More is 1000*M + 100*O + 10*R + E,
    Money is 10000 * M + 1000*O + 100*N + 10*E + Y,
    Money is Send + More,
    write(Send), write('+'), write(More), write('='), write(Money).

sendMoreMoneyAllDifferent(S,E,N,D,M,O,R,Y) :-
    chiffre(S),
    chiffre(E), S\=E,
    chiffre(N), S\=N, E\=N,
    chiffre(D), S\=D, E\=D, N\=D,
    chiffre(M), S\=M, E\=M, N\=M, D\=M, M\=0,
    chiffre(O), S\=O, E\=O, N\=O, D\=O, M\=O,
    chiffre(R), S\=R, E\=R, N\=R, D\=R, M\=R, O\=R,
    chiffre(Y), S\=Y, E\=Y, N\=Y, D\=Y, M\=Y, O\=Y, R\=Y,
    Send is S*1000 + E*100 + N*10 + D,
    More is M*1000 + O*100 + R*10 + E,
    Money is M*10000 + O*1000 + N*100 + E*10 + Y,
    Money is Send + More,
    write(Send), write('+'),write(More),write('='),write(Money).
