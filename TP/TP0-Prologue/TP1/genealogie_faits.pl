% faits
homme(albert).
homme(jean).
homme(paul).
homme(bertrand).
homme(louis).
homme(benoit).
homme(bob).
homme(tom).
homme(henri).
homme(riton).

femme(ginette).

femme(germaine).
femme(christiane).
femme(simone).
femme(marie).
femme(sophie).

pere(albert,jean).
pere(jean,paul).
pere(paul,bertrand).
pere(paul,sophie).
pere(jean,simone).
pere(louis,benoit).

pere(henri,bob).
pere(riton,tom).

mere(ginette,bob).
mere(ginette,tom).
mere(ginette,gege).

mere(germaine,jean).
mere(christiane,simone).
mere(christiane,paul).
mere(simone,benoit).
mere(marie,bertrand).
mere(marie,sophie).

parent(X,Y) :- pere(X,Y).
parent(X,Y) :- mere(X,Y).


