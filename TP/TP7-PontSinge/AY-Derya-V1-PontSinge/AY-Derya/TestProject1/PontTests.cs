// AY DERYA 
// J'ai fait les 4 jeux de test (3 de l'�nonc� + 1 random ) + rajouter des tests unitaires
// Programme fonctionel

/* Reformulation du prb 
 * Voici une reformulation du probl�me :

Un groupe de personnes doit traverser un fleuve en empruntant un pont suspendu. 
Le pont a une longueur de L m�tres et une capacit� de C kilos. 
Chaque personne a un poids P et peut se d�placer � une vitesse V qui lui est propre. 
Il est interdit de d�passer sur le pont, m�me si la personne devant est plus lente. 
Une personne ne peut s'engager sur le pont que si son poids ne d�passe pas la capacit� 
maximale du pont. Si la capacit� est d�pass�e, la personne doit attendre avant de s'engager.

 */
using Cowboys;

namespace TestProject1
{
    public class PontTests
    {
        [Fact]
        public void Test1()
        {
            // Exemple 1
            List<Cowboy> cowboys1 = new List<Cowboy>
            {
                new Cowboy { Weight = 70, Speed = 5 },
                new Cowboy { Weight = 80, Speed = 3 },
                new Cowboy { Weight = 60, Speed = 4 }
            };

            //double crossingTime1 = BruteForceTraverseCowboys(cowboys1);
            //Console.WriteLine("Exemple 1 - Temps de travers�e : " + crossingTime1);

            // Exemple 2
            List<Cowboy> cowboys2 = new List<Cowboy>
            {
                new Cowboy { Weight = 90, Speed = 2 },
                new Cowboy { Weight = 80, Speed = 4 },
                new Cowboy { Weight = 70, Speed = 3 },
                new Cowboy { Weight = 60, Speed = 5 }
            };

            //double crossingTime2 = BruteForceTraverseCowboys(cowboys2);
            //Console.WriteLine("Exemple 2 - Temps de travers�e : " + crossingTime2);

                
        }  
    } 
}