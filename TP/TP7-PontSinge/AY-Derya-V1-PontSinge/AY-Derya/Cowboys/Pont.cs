﻿// AY DERYA 
// J'ai fait les 4 jeux de test (3 de l'énoncé + 1 random ) + rajouter des tests unitaires
// Programme fonctionel

 /* Reformulation du prb 
  * Voici une reformulation du problème :

 Un groupe de personnes doit traverser un fleuve en empruntant un pont suspendu. 
 Le pont a une longueur de L mètres et une capacité de C kilos. 
 Chaque personne a un poids P et peut se déplacer à une vitesse V qui lui est propre. 
 Il est interdit de dépasser sur le pont, même si la personne devant est plus lente. 
 Une personne ne peut s'engager sur le pont que si son poids ne dépasse pas la capacité 
 maximale du pont. Si la capacité est dépassée, la personne doit attendre avant de s'engager.

  */
 using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cowboys
{
    public class Pont
    {
        public int Length { get; }
        public int WeightLimit { get; }

        public Pont(int length, int weightLimit)
        {
            Length = length;
            WeightLimit = weightLimit;
        }

        public double TraverseCowboys(List<Cowboy> cowboys)
        {
            Console.WriteLine("Liste des cowboys :");
            foreach (var cowboy in cowboys)
            {
                Console.WriteLine(cowboy);
            }

            double time = 0;
            double currentPosition = 0;
            double currentWeight = 0;
            List<Cowboy> waitingCowboys = new List<Cowboy>();

            while (cowboys.Count > 0 || waitingCowboys.Count > 0)
            {
                // Vérifier si des cowboys peuvent être ajoutés sur le pont
                while (cowboys.Count > 0 && currentWeight + cowboys[0].Weight <= WeightLimit)
                {
                    var cowboy = cowboys[0];
                    currentWeight += cowboy.Weight;
                    waitingCowboys.Add(cowboy);
                    cowboys.RemoveAt(0);

                }

                // Déplacer les cowboys actuellement sur le pont
                for (int i = 0; i < waitingCowboys.Count; i++)
                {
                    var cowboy = waitingCowboys[i];
                    double crossingTime = Length / (double)cowboy.Speed;

                    // Vérifier si la personne devant est plus lente
                    if (i > 0 && cowboy.Speed > waitingCowboys[i - 1].Speed)
                    {
                        double minArrivalTime = Math.Max(time + 1, currentPosition);
                        double maxCrossingTime = (waitingCowboys[i - 1].Speed * (minArrivalTime - currentPosition)) - currentPosition;
                        crossingTime = Math.Min(crossingTime, maxCrossingTime);
                    }

                    if (currentPosition + crossingTime <= time)
                    {
                        currentPosition += crossingTime;
                        currentWeight -= cowboy.Weight;
                        waitingCowboys.RemoveAt(i);
                        i--;
                    }
                }

                // Mettre à jour le temps en fonction du prochain cowboy sur le pont
                if (waitingCowboys.Count > 0)
                {
                    var nextCowboy = waitingCowboys[0];
                    double crossingTime = Length / (double)nextCowboy.Speed;
                    double expectedCrossingTime = Math.Max(time + 1, currentPosition) + crossingTime;
                    time = expectedCrossingTime;
                }
            }

            return time;
        }

        public double BruteForceTraverseCowboys(List<Cowboy> cowboys)
        {
            int n = cowboys.Count;
            double[] dp = new double[n];

            // Trier les cowboys par poids décroissant
            cowboys.Sort((x, y) => y.Weight.CompareTo(x.Weight));

            for (int i = 0; i < n; i++)
            {
                dp[i] = Length / cowboys[i].Speed;

                for (int j = 0; j < i; j++)
                {
                    double crossingTime = dp[j] + (Length / cowboys[i].Speed);
                    double maxCrossingTime = (cowboys[i].Speed * (dp[j] - Length)) / (cowboys[i].Speed - cowboys[j].Speed);
                    dp[i] = Math.Min(dp[i], Math.Max(crossingTime, maxCrossingTime));
                }
            }

            return dp[n - 1];
        }
    }
}
