﻿// AY DERYA 
// J'ai fait les 4 jeux de test (3 de l'énoncé + 1 random ) + rajouter des tests unitaires
// Programme fonctionel

 /* Reformulation du prb 
  * Voici une reformulation du problème :

 Un groupe de personnes doit traverser un fleuve en empruntant un pont suspendu. 
 Le pont a une longueur de L mètres et une capacité de C kilos. 
 Chaque personne a un poids P et peut se déplacer à une vitesse V qui lui est propre. 
 Il est interdit de dépasser sur le pont, même si la personne devant est plus lente. 
 Une personne ne peut s'engager sur le pont que si son poids ne dépasse pas la capacité 
 maximale du pont. Si la capacité est dépassée, la personne doit attendre avant de s'engager.

  */
 using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cowboys
{
    public class Cowboy
    {
        public int Speed { get; set; }
        public int Weight { get; set; }

        public override string ToString()
        {
            return $"({Speed}, {Weight})";
        }
    }
}
