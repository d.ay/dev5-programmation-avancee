﻿/* DERYA AY
 *  J'ai fait les 7 jeux de test (6 de l'énoncé + 1 jeu aléatoire)
 * 
 * 10 statégies mise en place :
 * - Faire passer les plus rapides en premier
 * - Faire passer les plus légers en premier
 * - Faire passer les cowboys dans l'ordre où ils se présentent
 * - Faire passer les plus lents en premier
 * - Faire passer les plus lourds en premier
 * - Faire passer les cowboys dans l'ordre du ratio poids/vitesse croissant
 * - Faire passer les cowboys dans l'ordre du ratio poids/vitesse décroissant
 * - Faire passer les cowboys dans l'ordre du ratio vitesse/poids croissant
 * - Faire passer les cowboys dans l'ordre du ratio vitesse/poids décroissant
 * - Brute force
 * 
 * Tout est fonctionnel sauf l'approche brute force que j'ai mise en commentaire
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Cowboys
{
    class Program
    {
        static (Pont pont, int N, List<Cowboy> cowboys) ReadDataFromFile(string filename)
        {
            var lines = File.ReadAllLines(filename);
            int L = int.Parse(lines[1]);
            int K = int.Parse(lines[3]);
            int N = int.Parse(lines[5]);
            Pont pont = new(L, K);
            List<Cowboy> cowboys = new List<Cowboy>();
            for (int i = 6; i < lines.Length; i += 2)
            {
                int weight = int.Parse(lines[i]);
                int speed = int.Parse(lines[i + 1]);
                cowboys.Add(new Cowboy { Speed = speed, Weight = weight });
            }
            return (pont, N, cowboys);
        }


        static (Pont pont, int N, List<Cowboy> cowboys) GenerateAleaData()
        {
            Random rand = new Random();
            int L = rand.Next(40, 80);
            int K = rand.Next(200, 400);
            int N = rand.Next(5, 30);
            Pont pont = new(L, K);
            List<Cowboy> cowboys = new List<Cowboy>();
            for (int i = 0; i < N; i++)
            {
                int weight = rand.Next(50, 120);
                int speed = rand.Next(1, 5);
                cowboys.Add(new Cowboy { Speed = speed, Weight = weight });
            }
            return (pont, N, cowboys);
        }


        static void RunStrategy(int strategyNum, string strategyName, List<Cowboy> cowboys, Pont pont, bool bruteForce)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Console.WriteLine($"\nStratégie {strategyNum} : {strategyName} ");
            double time = 0;
            
            if (bruteForce == false)
            {
                time = pont.TraverseCowboys(cowboys);
            }
            else
            {
                //time = pont.BruteForceTraverseCowboys(cowboys);  
            }
            
            Console.WriteLine($"\nTemps: {time}");
            // afficher le temps avec 2 chiffres après la virgule
            Console.WriteLine($"\nTemps: {time.ToString("F2")}");
            Console.WriteLine();
            stopwatch.Stop();
            Console.WriteLine($"Temps d'exécution : {stopwatch.Elapsed.TotalMilliseconds:F2} ms");
        }

        static void RunGame(Pont pont, int N, List<Cowboy> cowboys)
        {
            Console.WriteLine($"Pont: {pont.Length}m, Poids max: {pont.WeightLimit}kg, Nombre de cowboys: {N}");

            RunStrategy(1, "Faire passer les plus rapides en premier", cowboys.OrderByDescending(cowboy => cowboy.Speed).ToList(), pont, false);
            RunStrategy(2, "Faire passer les plus légers en premier", cowboys.OrderBy(cowboy => cowboy.Weight).ToList(), pont, false);
            RunStrategy(3, "Faire passer les cowboys dans l'ordre où ils se présentent", cowboys.ToList(), pont, false);
            RunStrategy(4, "Faire passer les plus lents en premier", cowboys.OrderBy(cowboy => cowboy.Speed).ToList(), pont, false);
            RunStrategy(5, "Faire passer les plus lourds en premier", cowboys.OrderByDescending(cowboy => cowboy.Weight).ToList(), pont, false);
            RunStrategy(6, "Faire passer les cowboys dans l'ordre du ratio poids/vitesse croissant", cowboys.OrderBy(cowboy => cowboy.Weight / cowboy.Speed).ToList(), pont, false);
            RunStrategy(7, "Faire passer les cowboys dans l'ordre du ratio poids/vitesse décroissant", cowboys.OrderByDescending(cowboy => cowboy.Weight / cowboy.Speed).ToList(), pont, false);
            RunStrategy(8, "Faire passer les cowboys dans l'ordre du ratio vitesse/poids croissant", cowboys.OrderBy(cowboy => cowboy.Speed / cowboy.Weight).ToList(), pont, false);
            RunStrategy(9, "Faire passer les cowboys dans l'ordre du ratio vitesse/poids décroissant", cowboys.OrderByDescending(cowboy => cowboy.Speed / cowboy.Weight).ToList(), pont, false);
            RunStrategy(10, "Brute force", cowboys.ToList(), pont, true);
        }

        static void Main(string[] args)
        {
            // Read data from files.
            for (int i = 1; i <= 6; i++)
            {
                Console.WriteLine($"\n\n*********************** Jeu CB{i} ****************************");
                var (pont, N, cowboys) = ReadDataFromFile($"jeu{i}.txt");
                RunGame(pont, N, cowboys);
                Console.WriteLine("************************************************************");
            }
            // Generate data for a random game.
            Console.WriteLine($"\n\n*********************** Jeu Aléa ****************************");
            var (pont2, N2, cowboys2) = GenerateAleaData();
            RunGame(pont2, N2, cowboys2);
            Console.WriteLine("************************************************************");
        }
    }
}
    