﻿/* DERYA AY
 *  J'ai fait les 7 jeux de test (6 de l'énoncé + 1 jeu aléatoire)
 * 
 * 10 statégies mise en place :
 * - Faire passer les plus rapides en premier
 * - Faire passer les plus légers en premier
 * - Faire passer les cowboys dans l'ordre où ils se présentent
 * - Faire passer les plus lents en premier
 * - Faire passer les plus lourds en premier
 * - Faire passer les cowboys dans l'ordre du ratio poids/vitesse croissant
 * - Faire passer les cowboys dans l'ordre du ratio poids/vitesse décroissant
 * - Faire passer les cowboys dans l'ordre du ratio vitesse/poids croissant
 * - Faire passer les cowboys dans l'ordre du ratio vitesse/poids décroissant
 * - Brute force
 * 
 * Tout est fonctionnel sauf l'approche brute force que j'ai mise en commentaire
 */

using Cowboys;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Cowboys
{
    public class Pont
    {
        public int Length { get; }
        public int WeightLimit { get; }

        public Pont(int length, int weightLimit)
        {
            Length = length;
            WeightLimit = weightLimit;
        }

        public double TraverseCowboys(List<Cowboy> cowboys)
        {
            double tempsTotal = 0.0;
            double total = 0.0;
            int poidsSurPont = 0;

            List<Cowboy> cowboysSurPont = new List<Cowboy>();
            List<Cowboy> cowboysEnAttente = new List<Cowboy>(cowboys);

            // tant qu'il reste des cowboys à faire traverser
            while (cowboysEnAttente.Count > 0)
            {
                Cowboy prochainCowboy = cowboysEnAttente.First();
                poidsSurPont = cowboysSurPont.Sum(c => c.Weight);

                // on ajoute le prochain cowboy si la capacité max du pont n'est pas atteint
                if (poidsSurPont + prochainCowboy.Weight <= WeightLimit)
                {
                    cowboysEnAttente.Remove(prochainCowboy);
                    cowboysSurPont.Add(prochainCowboy);

                    int index = cowboysSurPont.Count;

                    // calcul temps du cowboy
                    double tempsTraverse = Length / (double)prochainCowboy.Speed;
                    // calcul temps total avec attente
                    double tempsTraverseAvecAttente = tempsTotal + tempsTraverse;
                    double tempsSortieMax = 0.0;

                    // si y a un cowboy devant nous
                    if (index > 1)
                    {
                        for (int i = 0; i < index - 1; i++)
                        {
                            double tempsSortie = cowboysSurPont[i].TimeToCrossPont;
                            // si la personne devant est plus lente que nous
                            if (tempsSortie > tempsSortieMax)
                            {
                                tempsSortieMax = tempsSortie;
                            }
                        }
                    }
                    prochainCowboy.TimeToCrossPont = Math.Max(tempsTraverseAvecAttente, tempsSortieMax);
                }
                else
                {
                    // on supprime le cowboy qui sort du pont car il a fini la traversée
                    Cowboy premierCowboySurPont = cowboysSurPont.First();
                    cowboysSurPont.RemoveAt(0);
                    tempsTotal = premierCowboySurPont.TimeToCrossPont;

                }
            }
            // on prend le temps du dernier cowboy qui a fini de traverser le pont
            total = cowboysSurPont.Last().TimeToCrossPont;
            return total;
        }

        private List<List<Cowboy>> GeneratePermutations(List<Cowboy> cowboys)
        {
            Console.WriteLine("GeneratePermutations");
            List<List<Cowboy>> permutations = new List<List<Cowboy>>();
            GeneratePermutationsHelper(cowboys, new List<Cowboy>(), permutations);
            Console.WriteLine("GeneratePermutations - permutations.Count: " + permutations.Count);
            return permutations;
        }

        private void GeneratePermutationsHelper(List<Cowboy> cowboys, List<Cowboy> currentPermutation, List<List<Cowboy>> permutations)
        {
            if (cowboys.Count == 0)
            {
                // Ajouter une copie de la permutation actuelle à la liste des permutations
                permutations.Add(new List<Cowboy>(currentPermutation));
            }
            else
            {
                for (int i = 0; i < cowboys.Count; i++)
                {
                    Cowboy cowboy = cowboys[i];

                    // Ajouter le cowboy à la permutation actuelle
                    currentPermutation.Add(cowboy);
                    cowboys.RemoveAt(i);

                    // Appel récursif pour générer les permutations restantes
                    GeneratePermutationsHelper(cowboys, currentPermutation, permutations);

                    // Rétablir l'état initial en retirant le cowboy de la permutation actuelle
                    currentPermutation.RemoveAt(currentPermutation.Count - 1);
                    cowboys.Insert(i, cowboy);
                }
            }
        }

        public double BruteForceTraverseCowboys(List<Cowboy> cowboys)
        {
            Console.WriteLine("BruteForceTraverseCowboys");
            double meilleurTemps = double.MaxValue;

            foreach (List<Cowboy> permutation in GeneratePermutations(cowboys))
            {
                double tempsTotal = 0.0;
                double tempsSortieMax = 0.0;

                foreach (Cowboy cowboy in permutation)
                {
                    double tempsTraverse = Length / (double)cowboy.Speed;
                    double tempsTraverseAvecAttente = tempsTotal + tempsTraverse;

                    // Vérifier si le cowboy doit attendre la sortie des cowboys précédents
                    if (tempsSortieMax > tempsTotal)
                    {
                        tempsTraverseAvecAttente = Math.Max(tempsTraverseAvecAttente, tempsSortieMax);
                    }

                    cowboy.TimeToCrossPont = tempsTraverseAvecAttente;
                    tempsTotal = tempsTraverseAvecAttente;
                    tempsSortieMax = Math.Max(tempsSortieMax, tempsTraverseAvecAttente);
                }

                // Mettre à jour le meilleur temps
                if (tempsTotal < meilleurTemps)
                {
                    meilleurTemps = tempsTotal;
                }
            }

            return meilleurTemps;
        }
    }
}
