﻿/* DERYA AY
 *  J'ai fait les 7 jeux de test (6 de l'énoncé + 1 jeu aléatoire)
 * 
 * 10 statégies mise en place :
 * - Faire passer les plus rapides en premier
 * - Faire passer les plus légers en premier
 * - Faire passer les cowboys dans l'ordre où ils se présentent
 * - Faire passer les plus lents en premier
 * - Faire passer les plus lourds en premier
 * - Faire passer les cowboys dans l'ordre du ratio poids/vitesse croissant
 * - Faire passer les cowboys dans l'ordre du ratio poids/vitesse décroissant
 * - Faire passer les cowboys dans l'ordre du ratio vitesse/poids croissant
 * - Faire passer les cowboys dans l'ordre du ratio vitesse/poids décroissant
 * - Brute force
 * 
 * Tout est fonctionnel sauf l'approche brute force que j'ai mise en commentaire
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cowboys
{
    public class Cowboy
    {
        public int Speed { get; set; }
        public int Weight { get; set; }

        public double TimeToCrossPont { get; set; }

        public override string ToString()
        {
            return $"({Speed}, {Weight}, {TimeToCrossPont})";
        }
    }
}
