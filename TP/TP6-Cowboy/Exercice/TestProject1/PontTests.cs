
using Cowboys;

namespace TestProject1
{
    public class PontTests
    {
        [Fact]
        public void Test1()
        {
            // Arrange
            var cowboys = new List<Cowboy>() {
                new Cowboy() { Speed = 4, Weight = 110 },
                new Cowboy() { Speed = 2, Weight = 100 },
                new Cowboy() { Speed = 3, Weight = 90 },
                new Cowboy() { Speed = 1, Weight = 70 }
            };
            var pont = new Pont(80, 200);

            // Act
            var (rotations1, time1) = pont.TraverseCowboys(cowboys.OrderByDescending(cowboy => cowboy.Speed).ToList());
            var (rotations2, time2) = pont.TraverseCowboys(cowboys.OrderBy(cowboy => cowboy.Weight).ToList());
            var (rotations3, time3) = pont.TraverseCowboys(cowboys.ToList());
            var (rotations4, time4) = pont.TraverseCowboys(cowboys.OrderBy(cowboy => cowboy.Speed).ToList());
            var (rotations5, time5) = pont.TraverseCowboys(cowboys.OrderByDescending(cowboy => cowboy.Weight).ToList());

            // Assert
            Assert.Equal(2, rotations1);
            Assert.Equal(41.0, time1);
            Assert.Equal(2, rotations2);
            Assert.Equal(44.0, time2);
            Assert.Equal(3, rotations3);
            Assert.Equal(52.666666666666664, time3);
            Assert.Equal(3, rotations4);
            Assert.Equal(52.666666666666664, time4);
            Assert.Equal(3, rotations5);
            Assert.Equal(52.666666666666664, time5);
        }

        [Fact]
        public void Test2()
        {
            // Arrange
            var cowboys = new List<Cowboy>() {
                new Cowboy() { Speed = 4, Weight = 80 },
                new Cowboy() { Speed = 1, Weight = 110 },
                new Cowboy() { Speed = 3, Weight = 100 },
                new Cowboy() { Speed = 2, Weight = 70 }
            };
            var pont = new Pont(60, 150);

            // Act
            var (rotations1, time1) = pont.TraverseCowboys(cowboys.OrderByDescending(cowboy => cowboy.Speed).ToList());
            var (rotations2, time2) = pont.TraverseCowboys(cowboys.OrderBy(cowboy => cowboy.Weight).ToList());
            var (rotations3, time3) = pont.TraverseCowboys(cowboys.ToList());
            var (rotations4, time4) = pont.TraverseCowboys(cowboys.OrderBy(cowboy => cowboy.Speed).ToList());
            var (rotations5, time5) = pont.TraverseCowboys(cowboys.OrderByDescending(cowboy => cowboy.Weight).ToList());

            // Assert
            Assert.Equal(3, rotations1);
            Assert.Equal(51.0, time1);
            Assert.Equal(3, rotations2);
            Assert.Equal(54.0, time2);
            Assert.Equal(4, rotations3);
            Assert.Equal(65.0, time3);
            Assert.Equal(4, rotations4);
            Assert.Equal(65.0, time4);
            Assert.Equal(4, rotations5);
            Assert.Equal(65.0, time5);

        }
    }
}