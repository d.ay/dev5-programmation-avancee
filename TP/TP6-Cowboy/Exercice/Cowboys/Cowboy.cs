﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cowboys
{
    public class Cowboy
    {
        public int Speed { get; set; }
        public int Weight { get; set; }

        public override string ToString()
        {
            return $"({Speed}, {Weight})";
        }
    }
}
