﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cowboys
{
    public class Pont
    {
        public int Length { get; }
        public int WeightLimit { get; }

        public Pont(int length, int weightLimit)
        {
            Length = length;
            WeightLimit = weightLimit;
        }

        public (int rotations, double time) TraverseCowboys(List<Cowboy> cowboys)
        {
            Console.WriteLine("Liste des cowboys :");
            foreach (var cowboy in cowboys)
            {
                Console.WriteLine(cowboy);
            }

            int rotations = 0;
            double time = 0;
            while (cowboys.Any())
            {
                var group = new List<Cowboy>();
                int weight = 0;
                foreach (var cowboy in cowboys)
                {
                    if (weight + cowboy.Weight <= WeightLimit )
                    {
                        group.Add(cowboy);
                        weight += cowboy.Weight;
                        
                    }
                    else
                    {
                        break;
                    }
                }
                
                foreach (var cowboy in group)
                {
                    cowboys.Remove(cowboy);
                }
                
                double groupTime = Length / group.Min(cowboy => (double)cowboy.Speed);
                time += groupTime;
                if (cowboys.Any())
                {
                    Cowboy firstCowboy = group.First();
                    double firstCowboyTime = Length / (double)firstCowboy.Speed;
                    time += firstCowboyTime;
                    cowboys.Remove(firstCowboy);
                    cowboys.Insert(0, firstCowboy);
                }

                rotations++;
                Console.WriteLine($"Rotation {rotations}: {string.Join(", ", group.Select(cowboy => $"({cowboy.Speed}, {cowboy.Weight})"))}");
                Console.WriteLine($"Time: {time}");
            }
            return (rotations, time);
        }
    }
}
