﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cowboys
{
    class Program
    {

        static (Pont pont, int N, List<Cowboy> cowboys) ReadDataFromFile(string filename)
        {
            var lines = File.ReadAllLines(filename);
            int L = int.Parse(lines[1]);
            int K = int.Parse(lines[3]);
            int N = int.Parse(lines[5]);
            Pont pont = new(L, K);
            List<Cowboy> cowboys = new List<Cowboy>();
            for (int i = 6; i < lines.Length; i += 2)
            {
                int weight = int.Parse(lines[i]);
                int speed = int.Parse(lines[i + 1]);
                cowboys.Add(new Cowboy { Speed = speed, Weight = weight });
            }
            return (pont, N, cowboys);
        }


        static (Pont pont, int N, List<Cowboy> cowboys) GenerateAleaData()
        {
            Random rand = new Random();
            int L = rand.Next(40, 80);
            int K = rand.Next(200, 400);
            int N = rand.Next(5, 30);
            Pont pont = new(L, K);
            List<Cowboy> cowboys = new List<Cowboy>();
            for (int i = 0; i < N; i++)
            {
                int weight = rand.Next(50, 120);
                int speed = rand.Next(1, 5);
                cowboys.Add(new Cowboy { Speed = speed, Weight = weight });
            }
            return (pont, N, cowboys);
        }


        static void RunStrategy(int strategyNum, string strategyName, List<Cowboy> cowboys, Pont pont)
        {
            Console.WriteLine($"\nStratégie {strategyNum} : {strategyName} ");
            var (rotations, time) = pont.TraverseCowboys(cowboys);
            Console.WriteLine($"\nRotations: {rotations}, Temps: {time}");
            Console.WriteLine();
        }

        static void RunGame(Pont pont, int N, List<Cowboy> cowboys)
        {
            Console.WriteLine($"Pont: {pont.Length}m, Poids max: {pont.WeightLimit}kg, Nombre de cowboys: {N}");

            RunStrategy(1, "Faire passer les plus rapides en premier", cowboys.OrderByDescending(cowboy => cowboy.Speed).ToList(), pont);
            RunStrategy(2, "Faire passer les plus légers en premier", cowboys.OrderBy(cowboy => cowboy.Weight).ToList(), pont);
            RunStrategy(3, "Faire passer les cowboys dans l'ordre où ils se présentent", cowboys.ToList(), pont);
            RunStrategy(4, "Faire passer les plus lents en premier", cowboys.OrderBy(cowboy => cowboy.Speed).ToList(), pont);
            RunStrategy(5, "Faire passer les plus lourds en premier", cowboys.OrderByDescending(cowboy => cowboy.Weight).ToList(), pont);
        }

        static void Main(string[] args)
        {
            // Read data from files.
            for (int i = 1; i <= 4; i++)
            {
                Console.WriteLine($"\n\n*********************** Jeu CB{i} ****************************");
                var (pont, N, cowboys) = ReadDataFromFile($"jeuCB{i}.txt");
                RunGame(pont, N, cowboys);
                Console.WriteLine("************************************************************");
            }
            // Generate data for a random game.
            Console.WriteLine($"\n\n*********************** Jeu Aléa ****************************");
            var (pont2, N2, cowboys2) = GenerateAleaData();
            RunGame(pont2, N2, cowboys2);
            Console.WriteLine("************************************************************");
        }
    }
}
    